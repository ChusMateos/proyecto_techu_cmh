//includes. Variable y puerto donde tiene que escuchar
var express = require('express');
var app = express();
//módulo que tiene disponible body y le indicamos que use json
var bodyParser = require('body-parser');
app.use(bodyParser.json());
var port = process.env.PORT || 3000;

var baseMLabURL = "https://api.mlab.com/api/1/databases/apitechucmh/collections/";
var mLabAPIKey = "apiKey=2XhC9M0_12RPnFf9ic8BoMMZzSuMaPW-";
var requestJson = require('request-json');

//Inicializar app
app.listen(port);
console.log("API escuchando en el puerto " + port);

//el get devolvera una funcion que se le pasa como parametro
app.get ('/apitechu/v1',
  function(req, res) {
    console.log("GET /apitechu/v1");
    res.send(
      {
        "msg" : "Bienvenido a la API de Tech University"
      }
    );
  }
)

//Usuarios MongoDB
app.get('/apitechu/v2/users',
  function(req, res) {
    console.log("GET /apitechu/v2/users");

    httpCliente = requestJson.createClient(baseMLabURL);
    console.log("Buscando clientes");

    httpCliente.get("user?" + mLabAPIKey,
      function (err, resMLab, body){
          var response = !err ? body: {
            "msg" : "Error obteniendo usuarios"
          }
        res.send(response);
      }
  );
 }
)

// Usuarios  POR ID MongoDB

app.get('/apitechu/v2/users/:id',
  function(req, res) {
    console.log("GET /apitechu/v2/users/:id");

	var id = req.params.id;
	var query = 'q={"id" : ' + id + '}';

    httpCliente = requestJson.createClient(baseMLabURL);
    console.log("Cliente creado");

    httpCliente.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
	     if (err) {
	       response = {
		 "msg" : "Error obteniendo usuario."
	       }
	       res.status(500);
	     } else {
	       if (body.length > 0) {
		 response = body[0];
	       } else {
		 response = {
		   "msg" : "Usuario no encontrado."
		 };
		 res.status(404);
	       }
	     }
	     res.send(response);
	   }
  );
 }
)

//Nuevo Usuario MongoDB y alta cuenta
app.post('/apitechu/v2/newuser',
  function(req, res) {
   console.log("ALTA /apitechu/v2/newuser/");

  console.log("Email alta " + req.body.email);
  console.log("Password alta " + req.body.password);
  console.log("Nombre alta " + req.body.first_name);
  console.log("Apellido alta " + req.body.last_name);

  var email = req.body.email;
  var password = req.body.password;
  var first_name = req.body.first_name;
  var last_name = req.body.last_name;

//Busco email y si no lo encuentra da de alta
  var query = 'q={"email" : "' + email + '"}';

  httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente a obtener CHUS");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {

      console.log("y el body "+body.length);

     var idUser;
      if (body.length > 0) {
        var response = {
          "mensaje" : "Usuario encontrado"
        }
        res.send(response);
      } else {
        console.log("Buscando Todos los clientes");
        httpCliente = requestJson.createClient(baseMLabURL);
        httpCliente.get("user?s={'id': -1}&" + mLabAPIKey,
          function (errGET, resMLabGET, bodyGET){
             var response = !errGET ? bodyGET: {
                "msg" : "Error obteniendo usuarios"
                    }

            idUser = parseInt(bodyGET[0].id)+1;
            console.log("Ahora doy de alta con id siguiente ID"+idUser);

            var postBody = '{"id": ' + idUser + ', "email": "' + email + '", "password":"' + password +'", "first_name":"' + first_name +'", "last_name":"' + last_name +'"}';
            console.log("Usuario dado de alta");
            httpClient.post("user?"+ mLabAPIKey, JSON.parse(postBody),
                   function(errPOST, resMLabPOST, bodyPOST) {
                   console.log("POST done");

                    var iban = makeiban();
                    console.log("Este es el iban" + iban);
                    var balance = '0.00';

                    httpClient = requestJson.createClient(baseMLabURL);

                    var postBodyC = '{"userid": ' + idUser + ', "iban": "' + iban + '", "balance":"' + balance +'"}';
                    console.log("cuenta dada de alta");

                    httpClient.post("account?"+ mLabAPIKey, JSON.parse(postBodyC),
                           function(errPOSTC, resMLabPOSTC, bodyPOSTC) {
                           console.log("POST done");
                                response = {
                                      "msg" : "usuario y cuenta dada de alta.",
                                      "iban" : iban,
                                      "idUsuario" : idUser
                                    };
                                res.send(response);
                         }//Function post
                        );
                 }//Function post alta usuario
                );
              }
          ); //get user ordenar
        } //else
	 }//function get
 	);//Get
  }
)

//Modificar usuario
app.post('/apitechu/v2/changeuser',
  function(req, res) {
   console.log("MODIFICACION /apitechu/v2/changeuser/");

  console.log("Email mod " + req.body.email);
  console.log("Nueva contraseña alta " + req.body.newpassword);

  var email = req.body.email;
  var newpassword = req.body.newpassword; 

//Busco email y si no lo encuentra devuelve error
  var query = 'q={"email" : "' + email + '"}';

  httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente a obtener CHUS");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
      console.log("y el body "+body.length);
     
      if (body.length == 0) {
        var response = {
          "mensaje" : "Usuario no encontrado"
        }
        res.send(response);
      } else {        
         console.log("Nueva password: " + req.body.newpassword);
          //Hacemos el put de la password
          var queryPUT = 'q={"email" : "' + email + '"}';
  		  var putBody = '{"$set":{"password" :"' + req.body.newpassword + '"}}';
          console.log("Actualizando password");
  		  httpClient.put("user?"+ queryPUT + "&" + mLabAPIKey, JSON.parse(putBody),
  			   function(errPUT, resMLabPUT, bodyPUT) {
                console.log("PUT password done");
	     			response = {
				 	      "msg" : "Password actualizada."
			       		};
		     	res.send(response);
	   		 }//Function put
			);//put
            } //Else        
	 }//Funtion Get
 	);//Get
  }
)

//Eliminar usuario

//API CUENTAS
//Buscar cuenta por IDUSER

app.get('/apitechu/v2/users/:id/accounts',
  function(req, res) {
    console.log("GET /apitechu/v1/users/:id/accounts");

	var id = req.params.id;
	var query = 'q={"userid" : ' + id + '}';

    httpCliente = requestJson.createClient(baseMLabURL);
    console.log("Cliente creado en busqueda de cuentas");

    httpCliente.get("account?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
	     if (err) {
	       response = {
		         "msg" : "Error obteniendo cuentas."
	       }
	       res.status(500);
	     } else {
	       if (body.length > 0) {
		 response = body;
	       } else {
		 response = {
		   "msg" : "Usuario sin cuentas."
		 };
		 res.status(404);
	       }
	     }
	     res.send(response);
	   }
  );
}
)

//Consulta de saldo por cuenta para actualizar saldo en la ventana de Cuentas
//Buscar cuenta por IBAN y devolver el usuario

app.get('/apitechu/v2/accounts/:iban',
  function(req, res) {
    console.log("GET /apitechu/v2/accounts/:iban");

	var iban = req.params.iban;
	var query = 'q={"iban" : "' + iban + '"}';
    console.log("El IBAN a buscar es " + iban);

    httpCliente = requestJson.createClient(baseMLabURL);
    console.log("Cliente creado en busqueda de saldo por iban");

    httpCliente.get("account?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
	     if (err) {
	       response = {
		         "msg" : "Error obteniendo cuentas."
	       }
	       res.status(500);
	     } else {
	       if (body.length > 0) {
		 response = {
              "msg" : "Usuario de la cuenta.",
              "idUsuario" : body[0].userid
            };
	       } else {
		 response = {
		   "msg" : "Cuenta sin datos."
		 };
		 res.status(404);
	       }
	     }
	     res.send(response);
	   }
  );
}
)
//Alta de cuenta

function makeiban() {
  var iban = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 ";

  for (var i = 0; i < 20; i++)
    iban += possible.charAt(Math.floor(Math.random() * possible.length));
  return iban;
}

app.post('/apitechu/v2/newaccount/:id',
  function(req, res) {
   console.log("ALTA /apitechu/v2/newaccount/" + req.params.id);

    var iban = makeiban();
    console.log("Este es el iban" + iban);
    var balance = '0.00';

    httpClient = requestJson.createClient(baseMLabURL);

    var postBody = '{"userid": ' + req.params.id + ', "iban": "' + iban + '", "balance":"' + balance +'"}';
    console.log("cuenta dada de alta");

    httpClient.post("account?"+ mLabAPIKey, JSON.parse(postBody),
           function(errPOST, resMLabPOST, bodyPOST) {
           console.log("POST done");
                response = {
                      "msg" : "Cuenta dada de alta.",
                      "iban" : iban,
                        "id" : req.params.id
                    };
                res.send(response);
         }//Function post
        );
      }
)


//API MOVIMIENTOS

app.get('/apitechu/v2/movements',
  function(req, res) {
    console.log("GET /apitechu/v2/movements");

    httpCliente = requestJson.createClient(baseMLabURL);
    console.log("Buscando movimientos");

    httpCliente.get("movement?" + mLabAPIKey,
      function (err, resMLab, body){
          var response = !err ? body: {
            "msg" : "Error obteniendo movimientos"
          }
        res.send(response);
      }
  );
 }
)

//movimientos por cuenta ordenados por fecha

app.get('/apitechu/v2/accounts/:iban/movements',
  function(req, res) {
    console.log("GET /apitechu/v2/accounts/:iban/movements");

	var iban = req.params.iban;
	var query = 'q={"ibanmov": "' + iban + '"}';

    httpCliente = requestJson.createClient(baseMLabURL);
    console.log("busqueda de movimientos de la cuenta" + iban);

    httpCliente.get("movement?" + query + "&s={'date': -1}&" + mLabAPIKey,
      function(err, resMLab, body) {
	     if (err) {
	       response = {
		         "msg" : "Error obteniendo movimientos."
	       }
	       res.status(500);
	     } else {
	       if (body.length > 0) {
		 response = body;
	       } else {
		 response = {
		   "msg" : "Cuenta sin movimientos."
		 };
		 res.status(404);
	       }
	     }
        //console.log(response);
	     res.send(response);
	   }
  );
 }
)

//Alta movimientos y actualizar balance de la cuenta

app.post('/apitechu/v2/newmovement',
  function(req, res) {
   console.log("ALTA MOVIMIENTO /apitechu/v2/newmovement/");

    var f = new Date();
    var date = f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
    console.log("fecha de hoy" + date);

    var ibanmov = req.body.ibanmov;
    var amount = req.body.amount;
    var typemov = req.body.typemov;
    var date = date;

    httpClient = requestJson.createClient(baseMLabURL);

    var postBody = '{"ibanmov": "' + ibanmov + '","date": "' + date + '", "amount": "' + amount + '", "type":"' + typemov +'"}';
    console.log("movimiento dado de alta");
    httpClient.post("movement?"+ mLabAPIKey, JSON.parse(postBody),
           function(errPOST, resMLabPOST, bodyPOST) {
           console.log("POST done");
                response = {
                      "msg" : "movimiento dada de alta.",
                      "ibanmov" : ibanmov
                    };
        //recuperamos el balace de la cuenta y le sumamos el importe
        console.log("Buscamos la cuenta " + ibanmov);
        var queryc = 'q={"iban": "' + ibanmov + '"}';
        httpCliente = requestJson.createClient(baseMLabURL);
        console.log("Buscando cuenta para recuperar balance");

        httpCliente.get("account?" + queryc + "&" + mLabAPIKey,
        function (err, resMLab, bodyGET){
          var response = !err ? bodyGET: {
            "msg" : "Error obteniendo cuenta"
          }

          var balanceFinal;
          balanceFinal = parseFloat(bodyGET[0].balance) + parseFloat(amount);
          console.log("Balance final: " + balanceFinal);

          //Hacemos el put del balance
          var queryPUT = 'q={"iban" : "' + ibanmov + '"}';
  		  var putBody = '{"$set":{"balance" :' + balanceFinal + '}}';
          console.log("Actualizando balance");
  		  httpClient.put("account?"+ queryPUT + "&" + mLabAPIKey, JSON.parse(putBody),
  			   function(errPUT, resMLabPUT, bodyPUT) {
                console.log("PUT balance done");
	     			response = {
				 	      "msg" : "Balance actualizado.",
                          "balance" : balanceFinal,
                            "iban" : ibanmov
			       		};
		     	res.send(response);
	   		 }//Function put
			);
        }//function get
       );
    }//Function post
  );
}
)

//LOGIN API

app.post('/apitechu/v2/login',
  function(req, res) {
   console.log("LOGON /apitechu/v2/login/");

   console.log("Usuario logado" + req.body.email);
   console.log("Password logado" + req.body.password);

  var email = req.body.email;
  var password = req.body.password;

  var query = 'q={"email" : "' + email + '", "password" : "'+ password +'"}';

  httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente a obtener");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
      if (body.length == 0) {
        var response = {
          "mensaje" : "Logout incorrecto, usuario no encontrado Chus"
        }
        res.status(404);
        res.send(response);
      } else {
		  var queryPUT = 'q={"id" : ' + body[0].id + '}';
  		  var putBody = '{"$set":{"logged":true}}';
          console.log("Usuario se ha logado");
  		  httpClient.put("user?"+ queryPUT + "&" + mLabAPIKey, JSON.parse(putBody),
  			   function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
	     			response = {
				 	      "msg" : "Usuario logado.",
                          "idUsuario" : body[0].id
			       		};
		     		res.send(response);
	   		 }//Function put
			);//Put
        } //else
	 }//function get
 	);//Get
  }
)


//Logout MONGODB

app.post('/apitechu/v2/logout/:id',
 function(req, res) {
   console.log("POST /apitechu/v2/logout/:id");

   var query = 'q={"id": ' + req.params.id + '}';
   console.log("query es " + query);

   httpClient = requestJson.createClient(baseMLabURL);
   httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (body.length == 0) {
         var response = {
           "mensaje" : "Logout incorrecto, usuario no encontrado"
         }
         res.send(response);
       } else {
         console.log("Got a user with that id, logging out");
         query = 'q={"id" : ' + body[0].id +'}';
         console.log("Query for put is " + query);
         var putBody = '{"$unset":{"logged":""}}'
         httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
             console.log("PUT done");
             var response = {
               "msg" : "Usuario deslogado con éxito",
               "idUsuario" : body[0].id
             }
             res.send(response);
           }
         )
       }
     }
   );
 }
)


//Funcion Borrando con parametro
app.delete('/apitechu/v1/users/:id',
  function(req, res) {
    console.log("DELETE /apitechu/v1/users/:id");

    var users = require('./usuarios.json');
    users.splice(req.params.id - 1, 1);
    writeUserDataToFile(users);
    res.send(
      {
        "msg" : "Usuario borrado con exito"
      }
    );
  }
)

//Funcion que persiste los datos
function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);

  fs.writeFile(
    "./usuarios.json",
    jsonUserData,
    "utf8",
    function(err) {
        if (err) {
          console.log(err);
        } else {
          console.log("Fichero de usuarios persistido");
        }
    }
  );
}


//Funcion que manda cada tipo
app.post('/apitechu/v1/monstruo/:p1/:p2',
  function(req, res) {
    console.log("Parámetros");
    console.log(req.params);

    console.log("Query");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);
//Body solo con post
    console.log("Body");
    console.log(req.body);
  }
)
